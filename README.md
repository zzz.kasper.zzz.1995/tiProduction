# tiProduction


Для запуска программы потребуется установить:
- https://nodejs.org/en
- https://classic.yarnpkg.com/lang/en/docs/install/#mac-stable

Подтянуть зависимости
```
yarn
```

Так же для работы потребуется .env файл в котором прописаны токены
OPENAI_API_KEY=ВашКлючОтГпт
BOT_TOKEN=ВашТокенБота


## Запуск программы

```
yarn dev
```


### Настройка сообщений

Расписание настраивается тут src/configs/schedules.ts
Промты настраиваются тут src/configs/promptMap.ts

Целевой канал настраивается тут index.ts:17


### Docker

Дока https://docs.docker.com/reference/cli/docker/image/

Dockerfile - файл описывающий будующий образ


// Собрать образ
// Тут --file - путь до Dockerfile 
// . - директория с приложением
docker build -t for_ti_production_v2 --file .docker/Dockerfile .




// Посмотреть список образов
docker images

// Посмотреть список запущенных контейнеров
docker ps

// Запускает образ
// --env-file .env - передаем env файл необходимый для работы апп
docker run --env-file .env -d for_ti_production_v2

Название контейнера из докерхаба
//tiprodtest/testper


// Тегирование образа: Вы должны создать тег для вашего локального образа, который соответствует названию репозитория в Docker Hub
docker tag <existing-image>:<tag> <new-repo>:<new-tag>
docker tag for_ti_production_v2:latest tiprodtest/testper:latest

// загрузка образа в Docker Hub
docker push <new-repo>:<new-tag>
docker push tiprodtest/testper:latest

// Скачать докер
docker image pull tiprodtest/testper:latest

// Авторизация в dockerhub
// echo "Авторизация в dochub" | docker login --username мойлогин --password-stdin

