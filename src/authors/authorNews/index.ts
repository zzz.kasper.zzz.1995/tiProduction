import format from 'string-template'

import { TelegramBotService } from '../../services/telegramBotService';
import { GptProviderService } from '../../services/gptService';
import { TelegramListenerService } from '../../services/telegramListenerService';
import { TelegramMessageEvent } from '../../services/telegramListenerService/types';

import { config, promptMap } from './config';
import { isValidJSON, getMeta, Meta, log, getCurrencyRates, formatNumber } from './utils';

const IMAGE_SOURCES_FOLDER = '../../../imageSources'

interface MessageData {
  id: number,
  chanel: string;
  text?: string;
  file?: {
    media: {
      className?: "Photo" | "Document" | string
      [k: string]: unknown 
    }
  };
  media?: any;
  date: number;
  messageInfo?: any
  fwdFrom?: any
}

/** Автор новостного канала */
class AuthorNews {
  private botService: TelegramBotService | null;
  private gptService: GptProviderService | null;
  private telegramListener: TelegramListenerService

  constructor(telegramListener: TelegramListenerService) {
      this.telegramListener = telegramListener;

      this.botService = TelegramBotService.getInstance();
      this.gptService = GptProviderService.getInstance();

      log.info('AuthorNews has created')
  }

  run = () => {
    // Подписываемся на каналы
    this.telegramListener.listenToChannel(config.subscribedChannels.map(el => el.name), this.processMessage);

    const text = '#run AuthorNews run. Content from:' + config.subscribedChannels.map(el => el.name).join(', ')

    log.info(text)
    this.publishMessage(config.debugChannel, text).catch(err => console.error('Start message didn`t send, err:', err));

    this.toDoTasks()
  }

  /** Задачи которые нужно выполнять периодически */
  private toDoTasks = () => {
    const channel = config.targetChannel
    const CURRENCY_MESSAGE_ID = 35

    log.info('START TASK: "currency"', channel, ', message_id', CURRENCY_MESSAGE_ID)

    const template = ({btc, usd, eur}: Awaited<ReturnType<typeof getCurrencyRates>>) => 
      `$ - ${formatNumber(usd)}₽ | € - ${formatNumber(eur)}₽ | ₿ - ${formatNumber(btc)}$`

    let lastMessage = ''
    setInterval(async () => {
      try {
        const currency = await getCurrencyRates()
        if(!currency?.btc || !currency?.eur || !currency?.usd) {
          return
        }

        const message = template(currency)
        if(lastMessage === message) {
          return
        }
        lastMessage = message
    
        try {
          log.info('Курс валют обновлен! ', message)
          await this.botService!.editMessage(channel, CURRENCY_MESSAGE_ID, message);
        } catch(err) {
          log.error('AuthorNews, editMessage, err', err)
        }

      } catch (err) {
        console.error('toDoTasks "currency", err:', err)
      }
    }, 10 * 60 * 1000)
  }

  /** Достаем значимую информацию из перехваченного поста */
  private extractMessageData = (event: TelegramMessageEvent): MessageData => {
    log.info('AuthorNew.extractMessageData, file')
    const messageInfo = event?.message

    return {
      id: messageInfo.id,
      chanel: messageInfo?.chat?.username,
      text: messageInfo?.text,
      file: messageInfo?.file,
      media: messageInfo?.media,
      date: messageInfo?.date,
      fwdFrom: messageInfo.fwdFrom,
      messageInfo
    };
  }

  /** Обработка перехваченного поста */
  processMessage = async (event: TelegramMessageEvent): Promise<void> => {
    log.info('\n\nAuthorNew.processMessage get new content')

    const res = this.extractMessageData(event);
    const filePath = `${res.chanel}/${res.id}`
    let isPhoto = false

    try {
      if(res.file?.media?.className === 'Photo') {
        // TODO: Есть проблема, что скачиваем только 1 фото, а не все
        await this.telegramListener.downloadMedia(event.message, `./imageSources/${filePath}`)
        isPhoto = true
      }
    } catch {}

    try {
      log.info('AuthorNew.processMessage', res)
      if (!res.text) return;

      const filteredPost = this.filterPost(res);
      if (!filteredPost) return;

      const formattedText = await this.formatText(filteredPost);
      if (!formattedText) return;

      const meta = await this.getMeta(formattedText)
      if (!meta) return;

      const message = await this.rewriteMessage(formattedText, meta)
      // Публикуем новый пост
      await this.publishMessage(config.targetChannel, message, isPhoto ? filePath : undefined);

      // Отправляем отладочную информацию о посте
      await this.publishMessage(
        config.debugChannel,
        `${message} \n\n✅ Пост опубликован  \n\n${JSON.stringify(meta)}`,
        isPhoto ? filePath : undefined);
    } catch(err) {
      log.error('processMessage, err', err)

      await this.publishMessage(
        config.debugChannel,
        `${res.text}\n\n❌ err: *${err}*\nlen: *${res.text?.length}*`);
    }
  }

  private filterPost = (messageData: MessageData): MessageData | null => {
    const chanelConfig = config.subscribedChannels.find(el => el.name?.includes(messageData.chanel))

    const filterRules: { name: string; rule: (messageData: MessageData) => boolean }[] = [
      { name: 'MaxLength', rule: messageData => messageData.text!.length > 1200 },
      { name: 'MinLength', rule: messageData => messageData.text!.length < 20 },
      { name: 'ContainsRTVI', rule: messageData => messageData.text!.toLowerCase().includes('rtvi') },
      { name: 'ContainsHttp', rule: messageData => messageData.text!.includes('http') },
      { name: 'ContainsAd', rule: messageData => messageData.text!.includes('еклам') || messageData.text!.toLowerCase().includes('erid') },
      { name: 'IsRepost', rule: messageData => !!messageData.fwdFrom },
      { name: 'HasMedia', rule: messageData => !!messageData.media || !!messageData.file },
      { name: 'HasDocument', rule: messageData => (!!messageData.media || !!messageData.file) && messageData.file?.media?.className !== 'Photo' },
    ]

    const accessRules = filterRules.filter(el => (chanelConfig?.rules?.indexOf?.(el.name) ?? 0) >= 0)

    for (let { name, rule } of accessRules) {
      if (!messageData.text) {
        throw new Error('Сообщение пустое');
      }
      if (rule(messageData)) {
        throw new Error(`❌ Сообщение не прошло фильтрацию по правилу: ${name}`);
      }
    }

    return messageData;
  }

  /* Предформатируем текст */
  private formatText = async (messageData: MessageData): Promise<string | null> => {
    const initialText = messageData?.text || ''

    const formattingRules: ((text: string) => Promise<string>)[] = [
        async (text) => text.replace(/\s+/g, ' ').trim(),
        async (text) => text.replace(/\n{3}/, '\n\n'),
        async (text) => text.replace(`@${messageData?.chanel}`, ''),
        async (text) => text.replace(/\bруб\b\.?/gi, '₽')
      ];
  
      try {
        let currentText = initialText;
        for (const rule of formattingRules) {
          currentText = await rule(currentText);
        }
        
        return currentText;
      } catch (error) {
        log.info('Ошибка при форматировании текста:', error);

        return null;
      }
  }

  /** Получаем мета информацию о посте через GPT */
  private getMeta = async (text: string) => {
      const prompt = format(promptMap.scoring.command, {post: text})
      log.info('prompt', prompt)
      const refinedText = await this.gptService?.getContent(prompt, 'gpt-4');
      log.info('refinedText', refinedText)

      if(!isValidJSON(refinedText)) {
        throw new Error('Не удалось провести оценку текста')
      }

      const unapprovedMeta = JSON.parse(refinedText || '')
      const meta = {...unapprovedMeta, ...getMeta(unapprovedMeta)}

      if((meta?.interest || 0) < 3) {
        throw new Error('Новость скучная, meta: ' + JSON.stringify(meta))
      }
      if((meta?.positive || 0) < -3 && (meta?.important ?? 0) < 6) {
          throw new Error('Новость не позитивная, meta: ' + JSON.stringify(meta))
      }
      if((meta?.subjectivity || 0) > 5) {
        throw new Error('Новость субъективная, meta: ' + JSON.stringify(meta))
      }
      if(meta?.hasAds) {
        throw new Error('Новость содержит рекламу, meta: '+ JSON.stringify(meta))
      }
      if((meta?.important ?? 0) < 4) {
        throw new Error('Новость неважная, meta: '+ JSON.stringify(meta))
      }

      return meta;
  }

  /** Переписываем исходный текст */
  private rewriteMessage = async (text: string, meta?: Meta) => {
    try {
        if(!text) {
            return text
        }

        if(text?.length < 100) {
          return text
        }

        const prompt = format(promptMap.newsRetelling.command, {post: text})
        log.info('prompt', prompt)
        let refinedText = (await this.gptService?.getContent(prompt, 'gpt-4-0125-preview')) || '';
        log.info('refinedText', refinedText)

        if(meta?.title && refinedText.length > 350) {
          refinedText = `*${meta?.title}*\n\n${refinedText}`
          
          if((meta.important ?? 0) > 8) {
            refinedText = `⚡ ${refinedText}`
          }
        }

        return refinedText || '';
    } catch (err) {
        log.error('request to GPT failed, err:', err)
    }

    return  '';
  }

  /** Опубликовать пост */
  publishMessage = async (targetChannel: string, message: string, imagePath?: string): Promise<void> => {
    log.info('AuthorNew.publishMessage', targetChannel, message)
    if(this.botService === null) {
        log.error('AuthorNews', 'botService is null')
    }

    try {
      await this.botService!.sendMessage(targetChannel, message, imagePath ? [`${IMAGE_SOURCES_FOLDER}/${imagePath}`] : undefined);
    } catch(err) {
      log.error('AuthorNews, publishMessage, err', err)
    }
  }
}

export {AuthorNews};
