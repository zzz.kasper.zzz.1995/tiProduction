export function isValidJSON(jsonString?: string) {
    if(!jsonString) {
        return false
    }

    try {
      JSON.parse(jsonString);

      return true;
    } catch (e) {
      return false;
    }
}

  export type Meta = {
    /** от -10 до 10 */
    interest?: number,
    /** от -10 до 10 */
    positive?: number,
    /** от 0 до 10 */
    important?: number,
    /** от 0 до 10 */
    subjectivity?: number,
    hasAds?: boolean,
    quote?: string,
    title?: string,
    source?: string | '',
  }

export function getMeta(obj: any): Meta | null {
    if (typeof obj !== 'object' || obj === null) {
      return null;
    }
  
    const res: Meta = {}

    const isInterestValid = typeof obj.interest === 'number';
    if(isInterestValid) {
      res.interest = obj.interest
    }

    const isTitleValid = typeof obj.title === 'string';
    if(isTitleValid) {
      res.title = obj.title
    }

    const isSourceValid = typeof obj.source === 'string';
    if(isSourceValid) {
      res.source = obj.source
    }

    const isPositiveValid = typeof obj.positive === 'number';
    if(isPositiveValid) {
      res.positive = obj.positive
    }

    const isImportantValid = typeof obj.important === 'number';
    if(isImportantValid) {
      res.important = obj.important
    }

    const isSubjectivityValid = typeof obj.subjectivity === 'number';
    if(isSubjectivityValid) {
      res.subjectivity = obj.subjectivity
    }

    const hasAdsValid = typeof obj.hasAds === 'boolean';
    if(hasAdsValid) {
      res.hasAds = obj.hasAds
    }

    const quoteValid = typeof obj.quote === 'string';
    if(quoteValid) {
      res.quote = obj.quote
    }
  
    return res;
}

// https://telepathy.freedesktop.org/doc/telepathy-glib/telepathy-glib-debug-ansi.html#TP-ANSI-FG-RED:CAPS
const map = {
  blue: 34,
  red: 31
}

const colorize = (text: string, colorCode: keyof typeof map = 'blue') => {


  return `\x1b[${map[colorCode] || 36}m${text}\x1b[0m `
}

const timestamp = () => colorize(`[${new Date().toUTCString()}]`, 'blue')

export const log = {
  info: (...arg: [string, ...any]) => {
    const [message, ...extra] = arg
    console.log(`${timestamp()}${message}`, ...extra)
  },
  error: (...arg: [string, ...any]) => {
    const [message, ...extra] = arg
    console.error(`${timestamp()}${colorize(message, 'red')}`, ...extra)
  }
}

interface CurrencyRates {
  usd: number;
  eur: number;
  btc: number;
}

export async function getCurrencyRates(): Promise<CurrencyRates> {
  // Fetch BTC rate
  const btcResponse = await fetch("https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=usd");
  const btcData = await btcResponse.json();
  const btcRate = Math.round(btcData.bitcoin.usd*10)/10;

  // Fetch USD and EUR rates
  const moexResponse = await fetch("https://iss.moex.com/iss/statistics/engines/currency/markets/selt/rates.json?iss.meta=off");
  const moexData = await moexResponse.json();
  const usdRate = Math.round(moexData.cbrf.data[0][3] * 10) / 10;
  const eurRate = Math.round(moexData.cbrf.data[0][6] * 10) / 10;

  return {
    btc: btcRate,
    usd: usdRate,
    eur: eurRate
  };
}

/** Вставляет пробелы между разрядами числа */
export const formatNumber = (
  str: string | number | undefined,
  symbol = ' ',
 ): string => {
  if (Number.isNaN(Number(str))) {
   if (typeof str === 'string') {
    return str;
   }
 
   return '';
  }
 
  let res =  `${str}`.replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, `$&${symbol}`);
 
  res = `${res}`.replace('.', ',');
 
  return res;
 };