import { TelegramListenerService } from './services/telegramListenerService';
import {AuthorNews} from './authors/authorNews';

const version = process?.env?.npm_package_version;

async function main() {
    console.log('START version:', version)
    const telegramListener = TelegramListenerService.getInstance();
    await telegramListener.init();

    const authorNews = new AuthorNews(telegramListener);

    authorNews.run()
}

export { main };
