import { Telegraf } from "telegraf";
import { InputMediaPhoto } from "telegraf/typings/core/types/typegram";
import { readFileSync } from 'fs';
import * as path from 'path';

require("dotenv").config();

interface EditMessageOptions {
  disableWebPagePreview?: boolean;
  replyMarkup?: any;
}

type MessageSender = (channelId: string, message: string, imagePaths?: string[], options?: {disableNotification?: boolean, disable_web_page_preview?: boolean}) => Promise<void>;

type MessageEditor = (channelId: string, messageId: number, newText: string, options?: EditMessageOptions) => Promise<void>;

class TelegramBotService {
  private static instance: TelegramBotService;
  private bot: Telegraf;

  private constructor(token: string) {
    this.bot = new Telegraf(token);

    this.initializeBot();
  }

  public static getInstance(): TelegramBotService | null {
    const token = process.env.BOT_TOKEN;

    if (!token) {
      console.error("Error cant find token");

      return null;
    }

    if (!TelegramBotService.instance) {
      TelegramBotService.instance = new TelegramBotService(token);
    }

    return TelegramBotService.instance;
  }

  private initializeBot(): void {
    this.bot.launch().then(() => {
      console.log("Бот запущен");
    });
  }

  public editMessage: MessageEditor = async (channelId, messageId, newText) => {
    if (!channelId) {
      throw new Error("Channel ID is not set.");
    }
    if (!messageId) {
      throw new Error("Message ID is not set.");
    }
    if (!newText) {
      throw new Error("New text is not set.");
    }
    
    await this.bot.telegram.editMessageText(
      channelId,
      messageId,
      undefined,
      newText,
      {parse_mode: 'Markdown'},
    );
  };

  public sendMessage: MessageSender = async (channelId, message, imagePaths, options) => {
    if (!channelId) {
      throw new Error("Channel ID is not set.");
    }
    if (!message && (!imagePaths || imagePaths.length === 0)) {
      throw new Error("Message or image paths must be set.");
    }
    
    if (imagePaths && imagePaths.length > 0) {
      const MAX_MEDIA_COUNT = 10
      const mediaGroup: InputMediaPhoto[] = imagePaths.splice(0, MAX_MEDIA_COUNT).map((imagePath, index) => {
        const absolutePath = path.resolve(__dirname, imagePath);
        const photo: InputMediaPhoto = {
          type: 'photo',
          media: { source: readFileSync(absolutePath) },
          caption: index === 0 ? message : undefined,
          parse_mode: 'Markdown'
        };

        return photo;
      });

      await this.bot.telegram.sendMediaGroup(
        channelId,
        mediaGroup,
        {
          disable_notification: options?.disableNotification ?? true,
        }
      );
    } else if (message) {
      await this.bot.telegram.sendMessage(
        channelId,
        message,
        {
          parse_mode: 'Markdown',
          disable_notification: options?.disableNotification ?? true,
          disable_web_page_preview: true
        }
      );
    }
  };
}

export { TelegramBotService };
