import { TelegramClient } from "telegram";
import { StringSession } from "telegram/sessions";
import { NewMessage } from "telegram/events";
import * as input from "input";
import { LocalStorage } from "node-localstorage";
import * as fs from 'fs/promises';
import * as path from 'path';

import { TelegramMessageEvent } from "./types";

require("dotenv").config();

class TelegramListenerService {
  private static instance: TelegramListenerService;

  private client: TelegramClient;
  private storage: LocalStorage;
  private apiId: number = 28925178;
  private apiHash: string = "cf647cbda4a455ec8c9a16d9d226389c";
  private session: StringSession;

  constructor() {
    const storagePath = "./localStorage";
    this.storage = new LocalStorage(storagePath);
    const savedSession = this.storage.getItem("telegram_session") || process.env.TELEGRAM_SESSION;
    this.session = new StringSession(savedSession ? savedSession : "");
    this.client = new TelegramClient(this.session, this.apiId, this.apiHash, {
      connectionRetries: 5,
    });
  }

  public static getInstance(): TelegramListenerService {
    if (!TelegramListenerService.instance) {
      TelegramListenerService.instance = new TelegramListenerService();
    }

    return TelegramListenerService.instance;
  }

  async init() {
    console.log("Initializing Telegram client...");
    await this.client.start({
      phoneNumber: async () => await input.text("Please enter your number: "),
      password: async () => await input.text("Please enter your password: "),
      phoneCode: async () =>
        await input.text("Please enter the code you received: "),
      onError: (err) => console.log(err),
    });
    console.log("Telegram client initialized and connected.");

    // Сохранение сессии в localStorage
    const sessionString = this.client.session.save() as unknown as string;
    this.storage.setItem("telegram_session", sessionString);
  }

  async listenToChannel(
    channels: string[],
    callback: (event: TelegramMessageEvent) => Promise<void>,
  ) {
    const handler = (event: TelegramMessageEvent) => {
      if (event) {
        callback(event);
      }
    };

    this.client.addEventHandler(handler, new NewMessage({ chats: channels }));
  }

  async disconnect() {
    await this.client.disconnect();
  }

  /** Функция для загрузки фото */
  async downloadMedia(message: TelegramMessageEvent['message'], filePath = './imageSources/photo.jpg') {
    if (message.media && message.media.photo) {
      try {
        const directoryPath = path.dirname(filePath);
        try {
          // Проверяем доступность папки
          await fs.access(directoryPath);
        } catch {
          // Если папка не существует, создаем ее
          console.log('Папка не существует, создаем...');
          await fs.mkdir(directoryPath, { recursive: true });
        }
  
        const photo = message.media.photo;
        await this.client.downloadMedia(photo, { outputFile: filePath });
        console.log('Фото сохранено');
      } catch (err) {
        console.error('Фото не сохранено, err:', err);
      }
    }
  }
}





export { TelegramListenerService };
