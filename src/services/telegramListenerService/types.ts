export interface TelegramMessageEvent {
  message: {
    id: number,
    chat: {
      username: string;
    };
    text?: string;
    file?: { 
      media: {
        className?: "Photo" | "Document" | string
        [k: string]: unknown 
      }
    }; 
    media?: any;
    date: number;
    fwdFrom?: any
  };
}