class TextBuffer {
    private buffer: string[] = [];
    private readonly maxSize: number = 100;

    /** Метод для добавления строки в буфер */
    public add(text: string): void {
        // Если буфер заполнен, удаляем самую старую строку
        if (this.buffer.length >= this.maxSize) {
            this.buffer.shift();
        }
        this.buffer.push(text);
    }

    /** Метод для получения всего буфера */
    public getBuffer(): string[] {
        return this.buffer;
    }
}

export {TextBuffer}